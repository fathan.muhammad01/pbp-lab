import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class AnonymousScreen extends StatelessWidget {
  static const routeName = '/anonymous';

  AnonymousScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Anonymous Messages'),
      ),
      drawer: MainDrawer(),
      body: SafeArea(
        child: Container(
          color: Colors.green[300],
          padding: const EdgeInsets.all(5.0), // pads outside of question box
          child: Column(
            children: [
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(Icons.question_answer),
                      title: Text('Do you like listening to music?'),
                      subtitle: Text('I like bossa nova and shoegaze lol'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('ANSWER'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(Icons.question_answer),
                      title: Text('What\'s your view on politics?'),
                      subtitle: Text(
                          'It\'s a cruel world, so I\'d rather stay away from it.'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('ANSWER'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
