from django.urls import path

# import request functions
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend),
]
