from django.urls import path
# import request functions
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json),
]
