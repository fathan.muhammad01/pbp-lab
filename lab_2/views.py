from django.shortcuts import render
# importing for XML format
from django.http.response import HttpResponse
from django.core import serializers
# importing models
from .models import Note

def index(request):
    # load note model for HTML
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    # load note model for XML
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    # load note model for JSON
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
