from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=20)
    fromVar = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    message = models.TextField()
