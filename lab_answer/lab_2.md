## 1. Apakah perbedaan antara JSON dan XML?

* JSON adalah standar terbuka berbasis teks menggunakan bahasa meta yang digunakan untuk transfer data, sementara XML adalah bahasa markup yang digunakan untuk menyimpan data secara struktural dan terorganisir.
* JSON umumnya lebih ringan, ringkas dan mudah dibaca jika dibandingkan dengan XML yang cenderung lebih rumit.
* JSON mendukung penggunaan array, sementara XML tidak.
* JSON menggunakan ekstensi file .json, sementara XML menggunakan ekstensi file .xml.

## 2. Apakah perbedaan antara HTML dan XML?

HTML dan XML merupakan bahasa markup yang mana keduanya digunakan untuk membuat sebuah aplikasi dan halaman web, namun terdapat berbagai perbedaan mendasar:
* HTML bersifat statis karena digunakan untuk menampilkan data, sementara XML bersifat dinamis karena digunakan untuk memindahkan data. HTML bersifat non-case sensitive, sementara XML bersifat case sensitive.
* Tag HTML bersifat predefined, sementara tag XML bersifat user defined.
* Tag HTML digunakan untuk menampilkan data, sementara tag XML digunakan untuk mendeskripsikan data.
* Closing tag HTML tidak wajib, sementara closing tag XML diwajibkan.
* HTML memiliki ekstensi file .html, sementara XML memiliki ekstensi file .xml.
